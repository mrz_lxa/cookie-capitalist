import {getDocumentViewportSize, ISize} from "../utils/Utils";

/**
 * Class that handler "div viewport" resizing routine.
 * This enables to have a constant rendering resolution yet resizable game window screen.
 */
export class ViewportWrapper {
    private _viewport: HTMLElement;
    private _renderResolution: ISize;

    constructor(renderResolution: ISize, viewport: HTMLElement) {
        this._viewport = viewport;
        this._renderResolution = renderResolution;

        this._viewport.style.width = `${this._renderResolution.width}px`;
        this._viewport.style.height = `${this._renderResolution.height}px`;
        this._viewport.style["transformOrigin"] = "0 0";

        window.addEventListener("resize", this.resize.bind(this), false);
        this.resize();
    }

    public resize(): void{
        let documentViewportSize: ISize = getDocumentViewportSize(),

            xScale = documentViewportSize.width / this._renderResolution.width,
            yScale = documentViewportSize.height / this._renderResolution.height,

            viewportScale = Math.min(xScale, yScale),
            viewportTop = Math.round((documentViewportSize.height - (this._renderResolution.height * viewportScale)) / 2),
            viewportLeft = Math.round((documentViewportSize.width - (this._renderResolution.width * viewportScale)) / 2);

        // may also search if transform with different prefixes are in place
        // instead of using constant "transform" only
        // "webkit", "moz", "ms", "o"
        this._viewport.style["transform"] = `scale(${viewportScale})`;
        this._viewport.style.top = `${viewportTop}px`;
        this._viewport.style.left = `${viewportLeft}px`;
    }
}