import {IVisualEntity} from "./interface/IVisualEntity";
import {IRenderable} from "./interface/IRenderable";
import {IAssetState} from "./AssetEntity";
import {ButtonUI, IButtonUIConfig} from "./ui/ButtonUI";
import {IGameState} from "./main";


export enum BonusType{
    MANAGER = "manager_bonus",
    TRIPLE_INCOME = "triple_income"
}


export interface IBonusState {
    available: boolean;
    id: string;
    incomeId: string;
    cost: number;
    type: BonusType
}


export class BonusEntity implements IVisualEntity, IRenderable{

    private _id: string;
    private _uiConfig: IButtonUIConfig;
    private _ui: ButtonUI;

    private _gameState: IGameState;
    private _bonusState: IBonusState;
    private _assetState: IAssetState;



    constructor(id: string, uiConfig: IButtonUIConfig, gameState: IGameState) {
        this._id = id;
        this._uiConfig = uiConfig;

        // store whole gameState to gain access to balance
        // there is no reason to expose whole gamestate for this only
        // one way to bypass that is to introduce Redux-like actions
        this._gameState = gameState;


        // storing "state slices" to skip searching for it each time we need an access
        this._bonusState = gameState.bonusStates.find((bonusState:IBonusState)=>{
            return bonusState.id === this._id;
        });
        this._assetState = gameState.assetStates.find((asset:IAssetState)=>{
            return asset.id === this._bonusState.incomeId;
        });
    }


    init(parent: PIXI.Container, resources: Partial<Record<string, PIXI.LoaderResource>>, ticker: PIXI.Ticker): void {
        this._ui = new ButtonUI(this._uiConfig);
        this._ui.init(parent,resources,ticker);

        this._ui.setText(`${this._uiConfig.captionText}: ${this._bonusState.cost}`);

        this._ui.setClickCallback(()=>{
            if(!this._bonusState.available){
                return;
            }

            this._bonusState.available = false;
            this._gameState.balance -= this._bonusState.cost;

            switch (this._bonusState.type) {
                case BonusType.MANAGER:
                    this._assetState.hasManager = true;
                    break;
                case BonusType.TRIPLE_INCOME:
                    this._assetState.incomePerUnit *= 3;
                    break;
                default: throw Error("Can't award unknown bonus.");
            }
        });
    }


    render() {
        let canPurchase: boolean = this._gameState.balance >= this._bonusState.cost;

        this._ui.setEnabled(canPurchase && this._bonusState.available && this._assetState.amount > 0);
    }
}