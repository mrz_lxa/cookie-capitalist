import * as PIXI from "pixi.js";


/**
 * Simple interface for all visual entities on the screen.
 * TODO: here can be some system that provides proper UI parent/child inheritance
 * TODO: with configs inheritance etc
 */
export interface IVisualEntity {
    init(
        parent: PIXI.Container,
        resources: Partial<Record<string, PIXI.LoaderResource>>,
        ticker: PIXI.Ticker
    ): void;
}