/* Configure HTMLWebpack plugin */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
    template: "src/index.html",
    filename: "index.html",
    inject: "body"
});

/* Configure BrowserSync */
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const BrowserSyncPluginConfig = new BrowserSyncPlugin({
    host: 'localhost',
    port: 3000,
    proxy: 'http://localhost:9000/'
}, config = {
    reload: false
});

/* Configure ProgressBar */
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const ProgressBarPluginConfig = new ProgressBarPlugin();

const path = require("path");

/* Export configuration */
module.exports = (env) => {
    const config = {
        mode: 'development',
        devtool: 'source-map',
        entry: {
            "cookie-capitalist": "./src/ts/index.ts",
        },
        output: {
            path: path.join(__dirname, '/lib/'),
            filename: '[name].js',
            libraryTarget: "umd",
            library: 'cookie-capitalist'
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                },
            ]
        },
        resolve: {
            extensions: [".ts", ".js"]
        },
        plugins: []
    };


    if (env.dev) {
        // setup webpack-dev-server
        config.devServer = {
            static: {
                directory: path.join(__dirname),
            },
            compress: true,
            port: 9000,
        };
        config.output.path = path.join(__dirname, "/build/");
        config.plugins.push(HTMLWebpackPluginConfig, BrowserSyncPluginConfig, ProgressBarPluginConfig);
    } else {
        config.mode = "production";
        config.output.path = path.join(__dirname, "/dist/");
        //add minified entry for build script
        config.entry["cookie-capitalist.min"] = "./src/ts/index.ts";
    }

    return config;
};
